import React, { Component } from 'react';
import NumbersGame from './NumbersGame/NumbersGame.js';

import logo from './logo.svg';
import './css/App.css';

class App extends Component {
    render() {
        return (
            <div className="App">
                <div className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h2>Welcome to React</h2>
                </div>
                <p className="App-intro">
                    <NumbersGame />
                </p>
            </div>
        );
    }
}
export default App;
